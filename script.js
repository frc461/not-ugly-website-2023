$(document).ready(function() {
  const slideWrapper = $('.carousel-wrapper');
  const slides = $('.slide');
  const slideWidth = slides.first().width();
  const slideCount = slides.length;
  let currentIndex = 0;

  // Set initial position
  slideWrapper.css('transform', `translateX(${-currentIndex * slideWidth}px)`);

  // Previous button click event
  $('.prev-btn').on('click', function() {
    currentIndex = (currentIndex - 1 + slideCount) % slideCount;
    slideWrapper.css('transform', `translateX(${-currentIndex * slideWidth}px)`);
  });

  // Next button click event
  $('.next-btn').on('click', function() {
    currentIndex = (currentIndex + 1) % slideCount;
    slideWrapper.css('transform', `translateX(${-currentIndex * slideWidth}px)`);
  });
});
